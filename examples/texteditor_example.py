"""
Author: rdbende
License: GNU GPLv3
Copyright: 2021 rdbende
"""

import tkinter as tk
from tkinter import ttk

from tktext import TextEditor

root = tk.Tk()
root.title("CodeEditor example")

frame = ttk.Frame(root)
frame.pack(fill="both", expand=True)

text_editor = TextEditor(
    frame,
    width=40,
    height=10,
    autofocus=True,
    padx=10,
    pady=10,
)

text_editor.pack(fill="both", expand=True)

text_editor.content = """Type here and see the spall checking!"""


root.update()
root.minsize(root.winfo_width(), root.winfo_height())
root.mainloop()

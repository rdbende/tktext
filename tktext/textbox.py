"""
Author: rdbende
License: GNU GPLv3
Copyright: 2021 rdbende
"""

import os
import re
import tkinter as tk
from functools import partial
from tkinter import font as tkfont
from tkinter import ttk
from typing import Any

import enchant


class BaseTextBox(tk.Text):
    def __init__(
        self,
        master: tk.Misc,
        language: str,
        autofocus: bool,
        underlinecolor: str,
        **kwargs,
    ) -> None:
        # TODO: Explicitly write kwargs, for a better default-control
        hlthckns = kwargs.pop("highlightthickness", 0)
        wrap = kwargs.pop("wrap", "word")
        insertwidth = kwargs.pop("insertwidth", 1)

        self.frame = ttk.Frame(master)
        tk.Text.__init__(
            self,
            self.frame,
            highlightthickness=hlthckns,
            wrap=wrap,
            insertwidth=insertwidth,
            **kwargs,
        )

        tk.Text.grid(self, row=0, column=0, sticky="nsew")

        self._font = tkfont.Font(font=kwargs.pop("font", ("TkDeafultFont", 10)))

        self.configure(font=self._font)
        self.frame.grid_rowconfigure(0, weight=1)
        self.frame.grid_columnconfigure(0, weight=1)

        self._underlinecolor = underlinecolor
        self._language = language
        self.dictionary = enchant.Dict(self._language)

        self.menu = tk.Menu(self, tearoff=False)

        self.tag_configure("invalid", underline=True, underlinefg=self._underlinecolor)
        self.tag_bind("invalid", "<Button-3>", self.suggest)

        self._orig = self._w + "_orig"
        self.tk.call("rename", self._w, self._orig)
        self.tk.createcommand(self._w, self._proxy)

        if autofocus:
            self.focus()

    def _proxy(self, command, *args) -> Any:
        """Thanks to Bryan Oakley on StackOverflow: https://stackoverflow.com/a/40618152/"""
        cmd = (self._orig, command) + args
        result = self.tk.call(cmd)

        # Generate a <<ContentChanged>> event if the widget content was modified

        if command in {"insert", "replace", "delete"}:
            self.event_generate("<<ContentChanged>>")

        return result  # Returns what it would actually return

    def insert(self, index: str, content: str) -> str:
        line_no = int(
            self.index(index).split(".")[0]
        )  # Important! We don't want a text index like "end.end"

        if len(content.splitlines()) > 1:
            for line in content.splitlines():
                tk.Text.insert(self, f"{line_no}.end", line + "\n")
                self.mark_set("insert", f"{line_no}.end")
                self.check_line(line=line_no - 1)
                line_no += 1
        else:
            tk.Text.insert(self, index, content)
            self.check_line(line=line_no - 1)
        self.see(f"{line_no}.0")
        return "break"

    def check_line(self, event: tk.Event = None, line: int = None) -> None:
        """Checks the specified or the current line"""
        if line is None:
            line = int(self.index("insert").split(".")[0])
        line_text = self.get(f"{line}.0", f"{line}.end")
        start = f"{line}.0"

        for tag in self.tag_names(index=None):
            if tag != "sel":
                # Don't clear selection when pressing Ctrl + a
                # because this method runs on every keypress
                self.tag_remove(tag, f"{line}.0", f"{line}.end")

        for word in re.split(r" |\.|,|\?|;|:|\*|\"|'|\(|\)|\+|!|`|/|=", line_text):
            end = f"{start.split('.')[0]}.{int(start.split('.')[1]) + len(word)}"

            if word == "":  # Pyenchant don't like empty strings
                start = f"{end.split('.')[0]}.{int(end.split('.')[1]) + 1}"
                continue

            if not self.dictionary.check(word):
                self.tag_add("invalid", start, end)

            start = f"{end.split('.')[0]}.{int(end.split('.')[1]) + 1}"

    def check_all(self, *_) -> None:
        """Loops through the entire content and checks it"""
        for tag in self.tag_names(index=None):
            if tag != "sel":
                self.tag_remove(tag, "1.0", "end")

        for line in range(self.number_of_lines):
            self.check_line(line=line)

        self.event_generate("<<AllSpellChecked>>")

    def suggest(self, *_) -> None:
        """Pops up a menu with the correct options"""
        start, end = self.index("current wordstart"), self.index("current wordend")
        word = self.get(start, end)

        self.menu.delete(0, "end")
        self.menu.tk_popup(self.winfo_pointerx(), self.winfo_pointery())

        if self.dictionary.suggest(word):
            for suggestion in self.dictionary.suggest(word):
                self.menu.add_command(
                    label=suggestion,
                    command=partial(self.replace, start, end, suggestion),
                )
        else:
            self.menu.add_command(label="No suggestion", state="disabled")

    def load_from_file(self, file_name: str):
        """reads and inserts the content of a file into the textbox"""
        with open(file_name, "r") as file:
            self.clear()
            self.insert("end", file.read())
        self.check_all()
        self.event_generate("<<ContentLoadedFromFile>>")

    def save_to_file(
        self, file_name: str, start: str = "1.0", end: str = "end - 1 char"
    ):
        """Saves the content of the textbox to a file"""
        with open(file_name, "w") as file:
            file.write(self.get(start, end))
        self.event_generate("<<ContentSavedToFile>>")

    @property
    def content(self) -> str:
        return self.get("1.0", "end")

    @content.setter
    def content(self, new_content: str) -> None:
        self.clear()
        self.insert("end", new_content)

    @property
    def language(self) -> str:
        return self._language

    @language.setter
    def language(self, language) -> None:
        self.configure(language=language)

    @property
    def font_family(self) -> str:
        return self._font.actual("family")

    @font_family.setter
    def font_family(self, family: str) -> None:
        self._font.config(family=family)

    @property
    def font_size(self) -> int:
        return self._font.actual("size")

    @font_size.setter
    def font_size(self, size: int) -> None:
        self._font.config(size=size)

    @property
    def font(self) -> tuple:
        return self._font.actual()

    @property
    def number_of_lines(self) -> int:
        return int(self.index("end - 1 char").split(".")[0])

    @property
    def is_empty(self) -> bool:
        return self.get("1.0", "end") == "\n"

    def replace(self, start: str, end: str, content: str = "") -> None:
        self.delete(start, end)
        self.insert(start, content)

    def clear(self) -> None:
        self.delete("1.0", "end")

    def __setitem__(self, key, value):
        self.configure(**{key: value})

    def __getitem__(self, key: str):
        return self.cget(key)

    def __str__(self) -> str:
        return self.content

    def __repr__(self) -> str:
        result = f"{type(self).__module__}.{type(self).__name__} widget"

        if not self.winfo_exists():
            return f"<destroyed {result}>"

        return f"<{result}, language: {self._language!r}>"

    def keys(self) -> list:
        keys = tk.Text.keys(self)
        keys.extend(["autofocus", "language", "underlinecolor"])
        return sorted(keys)

    def cget(self, key: str):
        if key == "language":
            return self._language
        elif key == "underlinecolor":
            return self.underlinecolor
        else:
            return tk.Text.cget(self, key)

    def configure(self, **kwargs) -> None:
        lang = kwargs.pop("language", None)
        underlinecolor = kwargs.pop("underlinecolor", None)
        if lang:
            self._language = lang
            self.dictionary = enchant.Dict(self._language)
            self.check_all()
        if underlinecolor:
            self._underlinecolor = underlinecolor
            self.tag_configure("invalid", underlinefg=self._underlinecolor)
        tk.Text.configure(self, **kwargs)

    config = configure

    def pack(self, *args, **kwargs):
        self.frame.pack(*args, **kwargs)

    def grid(self, *args, **kwargs):
        self.frame.grid(*args, **kwargs)

    def place(self, *args, **kwargs):
        self.frame.place(*args, **kwargs)

    def destroy(self):
        """Destroys this widget"""
        # Explicit tcl calls are needed to avoid recursion error
        for i in self.frame.children.values():
            self.tk.call("destroy", i._w)
        self.tk.call("destroy", self.frame._w)

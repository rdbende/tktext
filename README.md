# tktext
## This is an experimental, hyper-super-beta-prototype-pre-release!!

A text editor widget for tkinter with spell checking with Pyenchant for many languages, plenty of property getters and setters, for easier handling, built-in read from or save to file, and many more coming soon...


### Screenshots

![image](https://user-images.githubusercontent.com/77941087/126702563-953351db-3719-494a-899f-131e58cd83d9.png)

